//
//  RowView.swift
//  TestWorkAlef
//
//  Created by muxammed on 08.11.2021.
//

import UIKit

class RowView: UIView {
    
    var viewController:ViewController?
    var child:Child?
    
    var humanName:InputView = {
        let inv = InputView()
        inv.topLine.text = "Имя"
        inv.translatesAutoresizingMaskIntoConstraints = false
        return inv
    }()
    
    var humanAge:InputView = {
        let inv = InputView()
        inv.topLine.text = "Возраст"
        inv.translatesAutoresizingMaskIntoConstraints = false
        return inv
    }()
    
    lazy var deleteRowButton:UIButton = {
        let button = UIButton()
        button.setAttributedTitle(NSAttributedString(string: "Удалить", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .regular)]), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.init(red: 47/255, green: 147/255, blue: 252/255, alpha: 1.0), for: .normal)
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(delItem), for: .touchUpInside)
        button.isUserInteractionEnabled = true
        return button
    }()
    
    @objc func delItem(button:UIButton){
        print("DELETE ETMELI")
        viewController?.deleteButtonChild(pos: child!)
    }
    
    var isChild:Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func setupViews(){
        
        self.addSubview(humanName)
        self.addSubview(humanAge)
        self.addSubview(deleteRowButton)
        
        setupCons()
        
        
    }
    
    fileprivate func setupCons() {
        if (isChild){
            
            
            let constraints:[NSLayoutConstraint] = [
                
                humanName.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
                humanName.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
                humanName.trailingAnchor.constraint(equalTo: self.deleteRowButton.leadingAnchor, constant: -16),
                humanName.heightAnchor.constraint(equalToConstant: 60),
                humanName.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/2, constant: -16),
                
                humanAge.topAnchor.constraint(equalTo: humanName.bottomAnchor, constant: 8),
                humanAge.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
                humanAge.heightAnchor.constraint(equalToConstant: 60),
                humanAge.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/2, constant: -16),
                
                deleteRowButton.leadingAnchor.constraint(equalTo: humanName.trailingAnchor, constant: 16),
                deleteRowButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
                deleteRowButton.centerYAnchor.constraint(equalTo: humanName.centerYAnchor),
                deleteRowButton.heightAnchor.constraint(equalToConstant: 30),
                
            ]
            
            NSLayoutConstraint.activate(constraints)
            
        } else {
            
            self.deleteRowButton.removeFromSuperview()
            
            let constraints:[NSLayoutConstraint] = [
                
                humanName.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
                humanName.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
                humanName.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
                humanName.heightAnchor.constraint(equalToConstant: 60),
                
                humanAge.topAnchor.constraint(equalTo: humanName.bottomAnchor, constant: 8),
                humanAge.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
                humanAge.trailingAnchor.constraint(equalTo:  self.trailingAnchor, constant: -16),
                humanAge.heightAnchor.constraint(equalToConstant: 60),
                
                
            ]
            
            NSLayoutConstraint.activate(constraints)
        }
    }
    
    
    override func layoutSubviews() {
        setupViews()
    }
    
}
