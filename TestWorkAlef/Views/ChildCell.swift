//
//  ChildCell.swift
//  TestWorkAlef
//
//  Created by muxammed on 09.11.2021.
//

import UIKit

class ChildCell: UICollectionViewCell {
    
    var viewController: ViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var childRow:RowView = {
        let cr = RowView()
        cr.isChild = true
        cr.translatesAutoresizingMaskIntoConstraints = false
        cr.viewController = viewController
        return cr
    }()
    
    let lineView : UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    func setupViews(){
        
        
        self.addSubview(lineView)
        self.addSubview(childRow)
        
        let cons:[NSLayoutConstraint] = [
            childRow.topAnchor.constraint(equalTo: topAnchor),
            childRow.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 0),
            childRow.trailingAnchor.constraint(equalTo: trailingAnchor,constant: 0),
            childRow.bottomAnchor.constraint(equalTo: lineView.topAnchor),
            
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            lineView.heightAnchor.constraint(equalToConstant: 0.5),
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4)
        ]
        
        NSLayoutConstraint.activate(cons)
    }
    
    override func prepareForReuse() {
        self.childRow.humanAge.valueField.text = ""
        self.childRow.humanName.valueField.text = ""
    }
}
