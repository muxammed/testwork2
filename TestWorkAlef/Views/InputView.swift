//
//  InputView.swift
//  TestWorkAlef
//
//  Created by muxammed on 07.11.2021.
//

import UIKit


class InputView: UIView {
    
    let topLine:UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.text = "Имя"
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let valueField:UITextField = {
        let txField = UITextField()
        txField.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        txField.text = ""
        txField.translatesAutoresizingMaskIntoConstraints = false
        return txField
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        self.layer.cornerRadius = 4
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.init(red: 240/255, green: 240/255, blue: 240/255, alpha: 1).cgColor
        
        self.addSubview(topLine)
        self.addSubview(valueField)
        
        let constrains:[NSLayoutConstraint] = [
            topLine.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            topLine.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
            topLine.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 16),
            
            valueField.topAnchor.constraint(equalTo: topLine.bottomAnchor, constant: 5),
            valueField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
            valueField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 16),
            
        ]
        
        NSLayoutConstraint.activate(constrains)
    }
    
}
