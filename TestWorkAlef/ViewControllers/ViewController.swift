//
//  ViewController.swift
//  TestWorkAlef
//
//  Created by muxammed on 07.11.2021.
//

import UIKit

struct Child {
    let name:String
    let age:Int
    let order:Int
}

class ViewController: UIViewController {

    var childs:[Child] = []
    let cellid = "cellid"
    var corder = 0
    
    let firstLine:UILabel = {
        let lb = UILabel()
        lb.text = "Персональные данные"
        lb.textColor = .black
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        return lb
    }()
    
    let parentView:RowView = {
        let pv = RowView()
        pv.isChild = false
        pv.translatesAutoresizingMaskIntoConstraints = false
        return pv
    }()
    
    let addChildLabel:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Дети (макс.5)"
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var addChildButton:UIButton = {
        let button = UIButton()
        button.setAttributedTitle(NSAttributedString(string: "Добавить ребенка", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .regular)]), for: .normal)
        button.setTitleColor(UIColor.init(red: 47/255, green: 147/255, blue: 252/255, alpha: 1), for: .normal)
        button.layer.cornerRadius = 25
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.init(red: 47/255, green: 147/255, blue: 252/255, alpha: 1).cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(addChildAction), for: .touchUpInside)
        button.setImage(UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(textStyle: .title2)), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        return button
        
    }()
    
    lazy var resetFormButton:UIButton = {
        let button = UIButton()
       
        button.setAttributedTitle(NSAttributedString(string: "Очистить", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .regular)]), for: .normal)
        button.setTitleColor(UIColor.init(red: 208/255, green: 59/255, blue: 62/255, alpha: 1), for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 25
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.init(red: 208/255, green: 59/255, blue: 62/255, alpha: 1).cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(resetButtonAction), for: .touchUpInside)
        return button
        
    }()
    
    lazy var childsCollection:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.register(ChildCell.self, forCellWithReuseIdentifier: cellid)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.isScrollEnabled = false
        
        return cv
    }()
    
    
    let scrollView:UIScrollView = {
        let sc = UIScrollView()
        sc.translatesAutoresizingMaskIntoConstraints = false
        return sc
    }()
    
    var childsCollectionHeighAnchor:NSLayoutConstraint?
    var resetButtonBottomAnchor:NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupViews()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom:  50 + 16 + view.safeAreaInsets.bottom , right: 0)
            resetButtonBottomAnchor?.constant = -(view.safeAreaInsets.bottom) + 16
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 50 + 16 + view.safeAreaInsets.bottom , right: 0)
            resetButtonBottomAnchor?.constant = -(view.safeAreaInsets.bottom  + keyboardViewEndFrame.height)
        }

        self.view.layoutIfNeeded()
        scrollView.scrollIndicatorInsets = scrollView.contentInset

    }
    
    @objc func resetButtonAction(){
    
        self.resetFormButton.isHidden = true
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let cancelActionButton = UIAlertAction(title: "Отмена", style: .cancel) { _ in
            print("Отмена")
            self.resetFormButton.isHidden = false
        }
        let resetActionButton = UIAlertAction(title: "Очистить", style: .destructive)
            { _ in
            self.resetFormButton.isHidden = false
            self.childs.removeAll()
            self.childsCollectionHeighAnchor?.constant = CGFloat(self.childs.count * 152)
            self.childsCollection.reloadData()
            
            self.parentView.humanAge.valueField.text = ""
            self.parentView.humanName.valueField.text = ""
            self.view.endEditing(true)
            self.addChildButton.isHidden = false
        }
        
        actionSheetController.addAction(cancelActionButton)
        actionSheetController.addAction(resetActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @objc func addChildAction(){
        
        childs.append(Child(name: "", age: 0, order: corder))
        childs.sort { (child, child2) -> Bool in
            child.order < child2.order
        }
        
        corder += 1
        let indexPaths = IndexPath(item: childs.count - 1, section: 0)
        childsCollection.insertItems(at: [indexPaths])
        childsCollectionHeighAnchor?.constant = CGFloat(childs.count * 152)
        
        if (childs.count) == 5 {
            addChildButton.isHidden = true
        }
    }
    
    
    var labelWidth:CGFloat = 0
    
    func setupViews(){
        
        self.view.backgroundColor = .white
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(scrollView)
        
        self.scrollView.addSubview(firstLine)
        self.scrollView.addSubview(parentView)
        self.scrollView.addSubview(addChildLabel)
        self.scrollView.addSubview(addChildButton)
        self.scrollView.addSubview(childsCollection)
        self.scrollView.addSubview(resetFormButton)
        
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: view.safeAreaInsets.bottom + 50 + 16 + 16, right: 0)
        scrollView.scrollIndicatorInsets =  UIEdgeInsets(top: 0, left: 0, bottom: view.safeAreaInsets.bottom + 50 + 16 + 16, right: 0)
        
        
        var rect: CGRect = addChildLabel.frame
        rect.size = (addChildLabel.text?.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold)]))!
        labelWidth = rect.width
        
        setupConstraints()
    }


    fileprivate func setupConstraints() {
        let constraints:[NSLayoutConstraint] = [
            
            scrollView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 8),
            scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            
            firstLine.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 16),
            firstLine.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            firstLine.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16),
            
            
            parentView.topAnchor.constraint(equalTo: firstLine.bottomAnchor, constant: 8),
            parentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            parentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            parentView.heightAnchor.constraint(equalToConstant: 144),
            
            addChildLabel.centerYAnchor.constraint(equalTo: addChildButton.centerYAnchor),
            addChildLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            addChildLabel.widthAnchor.constraint(equalToConstant: labelWidth),
            
            addChildButton.topAnchor.constraint(equalTo: parentView.bottomAnchor, constant: 8),
            addChildButton.leadingAnchor.constraint(equalTo: addChildLabel.trailingAnchor, constant: 8),
            addChildButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            addChildButton.heightAnchor.constraint(equalToConstant: 50),
            
            
            childsCollection.topAnchor.constraint(equalTo: addChildButton.bottomAnchor, constant: 8),
            childsCollection.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            childsCollection.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            childsCollection.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            
            
            
            resetFormButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 56),
            resetFormButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -56),
            resetFormButton.heightAnchor.constraint(equalToConstant: 50)
            
        ]
        
        resetButtonBottomAnchor = resetFormButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
        resetButtonBottomAnchor?.isActive = true
        
        childsCollectionHeighAnchor = childsCollection.heightAnchor.constraint(greaterThanOrEqualToConstant: 0)
        childsCollectionHeighAnchor?.isActive = true
        NSLayoutConstraint.activate(constraints)
    }
    
    
    func deleteButtonChild(pos: Child) {
        
        let pos2 = (childs.firstIndex(where: {$0.order == pos.order}))!
        childs.remove(at: pos2)
        childsCollectionHeighAnchor?.constant = CGFloat(152*childs.count)
        childsCollection.deleteItems(at: [IndexPath(item: pos2, section: 0)])
        
        if (childs.count) < 5 {
            addChildButton.isHidden = false
        }
        
    }
    
    
}



extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! ChildCell
        
        cell.childRow.deleteRowButton.tag = indexPath.item
        cell.childRow.viewController = self
        cell.childRow.child = childs[indexPath.item]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return childs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 152)
    }
}


